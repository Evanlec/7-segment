#include "esphomelib/application.h"
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"
#include "ArduinoJson.h"
#include <SPI.h>

using namespace esphomelib;

static const int GREEN_LED = A9;
static const int YELLOW_LED = A11;
static const int RED_LED = A10;
static const int WIFI_PIN = A6;

static const char *TAG = "timer.component";

class TimerSwitch : public switch_::Switch {
  public:
    TimerSwitch(const std::string &friendly_name) : switch_::Switch(friendly_name) {
    }

    void turn_on() override {
    }
    void turn_off() override {
    }

};

class Timer : public sensor::MQTTSensorComponent {
 private:
  int m_seconds = 0;
  bool m_paused = true;
  TimerSwitch * m_timer_switch;
  std::string m_name;
  uint32_t m_update_interval;
  Adafruit_7segment m_clock_display = Adafruit_7segment();
 public:
  Timer(const std::string &friendly_name, uint32_t update_interval, TimerSwitch *timer_switch)
    : sensor::MQTTSensorComponent(new sensor::Sensor(friendly_name)) {
      m_name = friendly_name;
      m_update_interval = update_interval;
      m_timer_switch = timer_switch;
      // this->set_retain(false);
      this->sensor_->set_unit_of_measurement("s");
      this->sensor_->set_accuracy_decimals(0);
      this->sensor_->clear_filters();
      // this->set_custom_command_topic("");
  }

  sensor::Sensor * get_sensor() {
    return this->sensor_;
  }
  TimerSwitch * get_switch() {
    return this->m_timer_switch;
  }

  void setup() override {
    m_clock_display.setBrightness(10);
    m_clock_display.begin(0x70);
    this->reset();
    display_time();
    this->subscribe(this->get_command_topic(), [&](const std::string &payload) {
      if (payload == "pause") {
        ESP_LOGV(TAG, "pause");
        this->pause();
      } else if (payload == "run") {
        ESP_LOGV(TAG, "run");
        this->run();
      } else if (payload == "reset") {
        ESP_LOGV(TAG, "reset");
        this->reset();
      }
    }, 2);
  }


  void send_state() {
    // this->send_json_message(this->get_state_topic(), [&](JsonBuffer &buffer, JsonObject &root) {
    //   std::string output;
    //   root["time"] = this->get_time();
    //   root["friendly_time"] = this->friendly_time();
    //   root["state"] = this->get_timer_state();
    //   root.printTo(output);
    //   return output;
    // }, 0, false);
    this->publish_state(this->get_time());
  }

  std::string get_timer_state() {
    return m_paused ? "pause" : "run";
  }

  void pause() {
    cancel_interval("timer");
    m_paused = true;
    this->m_timer_switch->write_state(! m_paused);
    this->send_state();
  }

  void run() {
    if (m_paused == true) {
      set_interval(std::string("timer"), m_update_interval, [this](){this->interval();});
      m_paused = false;
      this->m_timer_switch->write_state(! m_paused);
      this->send_state();
    }
  }

  void reset() {
    m_seconds = 0;
    this->pause();
    this->display_time();
  }

  void toggle_led(uint8_t pin) {
    digitalWrite(pin, !digitalRead(pin));
  }
  
  void led_on(uint8_t pin) {
    digitalWrite(pin, HIGH);
  }

  void led_off(uint8_t pin) {
    digitalWrite(pin, LOW);
  }

  void interval() {
    m_seconds+=1;
    this->sensor_->push_new_value(this->get_time());
    this->send_state();
    display_time();
  }

  void display_time() {
    const int minutes = m_seconds/60;
    const int seconds = m_seconds%60;
    m_clock_display.print(minutes*100+seconds);

    m_clock_display.drawColon(true);

    if (seconds < 10) {
      m_clock_display.writeDigitNum(3, 0);
    }
    if (minutes < 10) {
      m_clock_display.writeDigitNum(0, 0);
    }
    if (minutes < 1) {
      m_clock_display.writeDigitNum(1, 0);
    }
    m_clock_display.writeDisplay();
  }

  int get_time() {
    return this->m_seconds;
  }

  std::string friendly_time() {
    char out[10];
    const int minutes = m_seconds/60;
    const int seconds = m_seconds%60;
    sprintf(out, "%02d:%02d", minutes, seconds);
    return std::string(out);
  }

  std::string friendly_name() const override {
    return m_name;
  } 

  std::string component_type() const override {
    return "sensor";
  }

  void send_discovery(JsonBuffer &buffer, JsonObject &root, mqtt::SendDiscoveryConfig &config) override {};
  
  void send_initial_state() override {
    this->send_state();
  };

  std::string unit_of_measurement() {
    return "s";
  };

  bool is_internal() override {
    return false;
  };
};

void setup() {

  App.set_name("timer");
  App.init_log(115200);
  App.init_i2c(SDA, SCL);

  // Wifi selector
  pinMode(WIFI_PIN, INPUT_PULLUP);
  auto wifi_pin_value = digitalRead(WIFI_PIN);

  ESP_LOGI(TAG, "wifi pin %i value: %i", WIFI_PIN, wifi_pin_value);

  // other wifi networks
  // App.init_wifi("EvanPixelHotspot", "Sunshine");
  // App.init_wifi("Localytics Guest", "appmarketing");

  if (wifi_pin_value == LOW) {
    App.init_wifi("dd-wrt", "Sunshine");
  } else {
    App.init_wifi("oldsouth-guest", "OSC-guest-1669");
  }

  // App.init_ota()->start_safe_mode();
  // auto web_server = App.init_web_server(80);

  // App.init_mqtt("m12.cloudmqtt.com", 17718, "esp32", "password");
  App.init_mqtt("mqtt.elecompte.info", 1883, "timer", "4v4KW0#UNU");

  // doesnt seem to report anything besides 1.10V
  // App.make_adc_sensor("Battery Voltage", A13);

  // App.make_restart_switch("restart timer");

  auto *timer_switch = new TimerSwitch("Pause/Start");
  auto *my_timer = new Timer("My Timer", 1000, timer_switch);

  auto *green_led_output = App.make_gpio_output(GREEN_LED);
  App.make_binary_light("Green LED", green_led_output);
  auto *yellow_led_output = App.make_gpio_output(YELLOW_LED);
  App.make_binary_light("Yellow LED", yellow_led_output);
  auto *red_led_output = App.make_gpio_output(RED_LED);
  App.make_binary_light("Red LED", red_led_output);

  App.make_status_led(LED_BUILTIN);
  
  App.register_component(my_timer);
  // web_server->register_sensor(my_timer->get_sensor());
  // web_server->register_switch(my_timer->get_switch());

  App.setup();
}

void loop() {
  App.loop();
}